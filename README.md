Uberbag is an Ubuntu Touch client for the pocket Web application written in QML/Qt. Pocket allows saving Web pages one stumbles upon for later reference. The client works on phones as well as on bigger screens. It allows reading a user's pocket web pages, mark favorites, mark them as read and delete them.

Uberbag is not in any way affiliated with Read It Later Inc.