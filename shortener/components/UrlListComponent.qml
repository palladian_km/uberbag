import QtQuick 2.0
import Ubuntu.Components 0.1
import Ubuntu.Components.ListItems 0.1
import "../model"

ListView {
    clip: true
    model: UrlListModel {}
    delegate: Standard {
        text: shortenedUrl
        control: ActivityIndicator {
            running: loading
        }

        onClicked: {
            Clipboard.push(shortenedUrl);
        }
    }

    function indicateLoading() {
        model.insert(0, {"loading":true, "shortenedUrl":"Loading...", "url":""});
    }

    function addUrl(url, shortenedUrl) {
        var firstItem = model.get(0);
        if (firstItem.loading) {
            firstItem.loading = false;
            firstItem.shortenedUrl = shortenedUrl;
            firstItem.url = url;
        } else {
            model.insert(0, {"loading":false, "shortenedUrl":shortenedUrl, "url":url});
        }
    }
}
