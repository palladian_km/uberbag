import QtQuick 2.0
import Ubuntu.Components 0.1
import Ubuntu.Unity.Action 1.0 as UnityActions
import "components"
import "model"

/*!
    \brief MainView with a Label and Button elements.
*/

MainView {
    // objectName for functional testing purposes (autopilot-qt5)
    objectName: "mainView"

    // Note! applicationName needs to match the "name" field of the click manifest
    applicationName: "com.ubuntu.developer.ubuntudroid.atomurl"

    /*
     This property enables the application to change orientation
     when the device is rotated. The default is false.
    */
    //automaticOrientation: true

    width: units.gu(40)
    height: units.gu(80)

    function shorten() {
        urlList.indicateLoading();
        shortenedUrlModel.postData = "{\"longUrl\":\"".concat(inputUrl.text).concat("\"}");
        shortenedUrlModel.refresh();
    }

//    actionManager: {
//        actions: [
//            new Action {
//                id: shortenAction
//                name: "Shorten"
//                onTriggered: {
//                    inputUrl.text = Clipboard.data.text;
//                    shorten();
//                }
//            }
//        ]
//    }

    actions: [
        Action {
            id: shortenAction
            name: "Shorten"
            onTriggered: {
                inputUrl.text = Clipboard.data.text;
                shorten();
            }
        }
    ]

    Page {
        title: i18n.tr("AtomUrl")

        JSONListModel {
            id: shortenedUrlModel
            source: "https://www.googleapis.com/urlshortener/v1/url"
            contentType: "application/json"
            requestType: "POST"
            query: "$"

            onListModelUpdateChanged: {
                var shortUrl = model.get(0).id;
                Clipboard.push(shortUrl);
                urlList.addUrl(model.get(0).longUrl, shortUrl);
            }
        }

        Column {
            id: topColumn
            spacing: units.gu(1)
            anchors {
                margins: units.gu(2)
                fill: parent
            }

            TextField {
                id: inputUrl
                // regex for URL courtesy of http://stackoverflow.com/a/14314836/304266
                validator: RegExpValidator { regExp: /^(http[s]?:\/\/(www\.)?|ftp:\/\/(www\.)?|(www\.)?){1}([0-9A-Za-z-\.@:%_\‌​+~#=]+)+((\.[a-zA-Z]{2,3})+)(\/(.)*)?(\?(.)*)?/g }
                width: parent.width
                focus: true

                text: 'www.ubuntu.com'
                font.pixelSize: FontUtils.sizeToPixels("medium")

                onAccepted: {
                    shorten()
                }
            }

            Button {
                id: shortenButton
                objectName: "button"
                width: parent.width

                text: i18n.tr("Shorten!")

                onClicked: {
                    shorten()
                }
            }

            UrlListComponent {
                id: urlList
                width: parent.width
                height: units.gu(100)
            }
        }
    }
}
