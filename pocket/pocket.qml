import QtQuick 2.0
//import QtWebKit 3.0
import Ubuntu.Components 1.1
import Ubuntu.Components.Popups 1.0
import Ubuntu.Components.Extras.Browser 0.2
import "components"
import "json/tools.js" as JSTools

/*!
    \brief MainView with a Label and Button elements.
*/

MainView {
    id: mainView
    // objectName for functional testing purposes (autopilot-qt5)
    objectName: "mainView"

    // Note! applicationName needs to match the "name" field of the click manifest
    applicationName: "com.ubuntu.developer.alaak.uberbag"

    /*
     This property enables the application to change orientation
     when the device is rotated. The default is false.
    */
    //automaticOrientation: true

    width: units.gu(300)
    height: units.gu(100)

    property string requestToken: "";
    property bool isRequested: false;
    property string accessToken: "";
    property string username: "";
    property bool isAuthenticated: false;
    property string apiKey: "24758-96f4f9ca2d5918b0cd65fa0d";

    property ItemListView listView;
    property DetailView detailView;
    property bool isMobileLayout : width <= units.gu(110);



    PageStack {
        id: pageStack

        Component.onCompleted: {
            console.error("Hello World!");
            push(page0);
        }

        Page {
            id:page0
            title: i18n.tr("Uberbag")

            Item {
                id: pocketOAuth
                anchors.fill: parent
                Component.onCompleted: {
                    if(isRequested) {
                        loginView.url = "https://getpocket.com/auth/authorize?request_token="+requestToken+"&redirect_uri=uberbag:authorizationFinished"
                        loginView.visible = true;
                    } else {
                        var req = new XMLHttpRequest();
                        req.onreadystatechange = function() {
                            if(req.readyState === XMLHttpRequest.DONE) {
                                var result = JSON.parse(req.responseText);
                                if(!result.error) {
                                    mainView.requestToken = result.code;
                                    mainView.isRequested = true;
                                    loginView.url = "https://getpocket.com/auth/authorize?request_token="+requestToken+"&redirect_uri=uberbag:authorizationFinished";
                                    loginView.visible = true;
                                }
                            }
                        }
                        req.open("post","https://getpocket.com/v3/oauth/request");
                        req.setRequestHeader("Content-Type","application/json; charset=UTF-8");
                        req.setRequestHeader("X-Accept","application/json");
                        req.send('{"consumer_key":"' + apiKey + '","redirect_uri":"ubupocket:authorizationFinished"}');
                    }
                }

                UbuntuWebView {
                    id:loginView
                    visible: false
                    anchors.fill: parent

                    preferences.standardFontFamily: "Ubuntu"

                    onUrlChanged: {
                        console.log(url.toString() + "##########");
                        console.log(url.toString()==="uberbag:authorizationFinished");
                        if(url.toString()==="uberbag:authorizationFinished") {
                            var req = new XMLHttpRequest();
                            req.onreadystatechange = function() {
                                if(req.readyState === XMLHttpRequest.DONE) {
                                    var result = JSON.parse(req.responseText);
                                    if(!result.error) {
                                        pageStack.push(page1);
                                        mainView.accessToken = result.access_token;
                                        mainView.username = result.username;
                                        mainView.isAuthenticated = true;
                                    }
                                }
                            }

                            req.open("post","https://getpocket.com/v3/oauth/authorize");
                            req.setRequestHeader("Content-Type","application/json; charset=UTF-8");
                            req.setRequestHeader("X-Accept","application/json");
                            req.send('{"consumer_key":"24758-96f4f9ca2d5918b0cd65fa0d","code":"'+requestToken+'"}');
                        }
                    }
                }
            }
        }

        Page {
            id: page1
            title: i18n.tr("Uberbag")
            visible: false

            tools: ToolbarItems {
                ToolbarButton {
                    action: Action {
                        text: i18n.tr("Refresh")
                        //iconSource: Qt.resolvedUrl("/usr/share/icons/gnome/scalable/actions/view-refresh-symbolic.svg")
                        iconName: "reload"
                        onTriggered: listView.refresh()
                    }
                }
                ToolbarButton {
                    action: Action {
                        text: i18n.tr("Add")
                        //iconSource: Qt.resolvedUrl("/usr/share/icons/gnome/scalable/actions/list-add-symbolic.svg")
                        iconName: "add"
                        onTriggered: addItem(Clipboard.data.text)
                    }
                }
                ToolbarButton {
                    action: Action {
                        text: i18n.tr("Mark read")
                        //iconSource: Qt.resolvedUrl("/usr/share/icons/gnome/scalable/actions/object-select-symbolic.svg")
                        iconName: "select"
                        onTriggered: {
                            if (isMobileLayout) {
                                archiveItem(detailPage.itemId);
                            } else {
                                archiveItem(detailView.itemId);
                            }
                        }
                    }
                    visible: !isMobileLayout
                }
                ToolbarButton {
                    action: Action {
                        text: i18n.tr("Delete")
                        //iconSource: Qt.resolvedUrl("/usr/share/icons/gnome/scalable/actions/edit-delete-symbolic.svg")
                        iconName: "delete"
                        onTriggered: {
                            if (isMobileLayout) {
                                deleteItem(detailPage.itemId);
                            } else {
                                deleteItem(detailView.itemId);
                            }
                        }
                    }
                    visible: !isMobileLayout
                }
                ToolbarButton {
                    action: Action {
                        text: i18n.tr("Favor")
                        //iconSource: Qt.resolvedUrl("/usr/share/icons/gnome/scalable/actions/bookmark-new-symbolic.svg")
                        iconName: "favorite-unselected"
                        onTriggered: {
                            if (isMobileLayout) {
                                favoriteItem(detailPage.itemId);
                            } else {
                                favoriteItem(detailView.itemId);
                            }
                        }
                    }
                    visible: !isMobileLayout
                }
                back.visible: false
            }

            onWidthChanged: {
                if (isMobileLayout === false) {
                    listView.width = width / 3;
                } else {
                    listView.width = width;
                }
            }
        }

        DetailPage {
            id: detailPage
            anchors.fill: parent

            tools: ToolbarItems {
                ToolbarButton {
                    action: Action {
                        text: i18n.tr("Mark read")
                        //iconSource: Qt.resolvedUrl("/usr/share/icons/gnome/scalable/actions/object-select-symbolic.svg")
                        iconName: "select"
                        onTriggered: archiveItem(detailPage.itemId)
                    }
                }
                ToolbarButton {
                    action: Action {
                        text: i18n.tr("Delete")
                        //iconSource: Qt.resolvedUrl("/usr/share/icons/gnome/scalable/actions/edit-delete-symbolic.svg")
                        iconName: "delete"
                        onTriggered: deleteItem(detailPage.itemId)
                    }
                }
                ToolbarButton {
                    action: Action {
                        text: i18n.tr("Favor")
                        //iconSource: Qt.resolvedUrl("/usr/share/icons/gnome/scalable/actions/bookmark-new-symbolic.svg")
                        iconName: "favorite-unselected"
                        onTriggered: favoriteItem(detailPage.itemId)
                    }
                }
                visible: isMobileLayout
            }
        }
    }

    onIsMobileLayoutChanged: {
        console.log("isMobileLayout changed to " + isMobileLayout);
        if (isMobileLayout === false) {
            listView.width = listView.parent.width / 3;
            if (detailView !== null ) {
                detailView.destroy();
            }
            var detailViewComponent = Qt.createComponent("components/DetailView.qml");
            detailView = detailViewComponent.createObject(page1);
            detailView.anchors.left = listView.right;
            detailView.anchors.right = detailView.parent.right;
            detailView.anchors.top = detailView.parent.top;
            detailView.anchors.bottom = detailView.parent.bottom;
            if (pageStack.currentPage == detailPage) {
                detailView.givenUrl = detailPage.givenUrl;
                detailView.itemId = detailPage.itemId;
                pageStack.pop();
            }
        } else {
            if (detailView !== null ) {
                detailView.destroy();
            }
            if (detailView.givenUrl !== null && detailView.givenUrl !== "") {
                pageStack.push(detailPage);
                detailPage.givenUrl = detailView.givenUrl;
                detailPage.itemId = detailPage.itemId;
            }
        }
    }

    onAccessTokenChanged: {
        if(accessToken !== "") {
            var listViewComponent = Qt.createComponent("components/ItemListView.qml");
            listView = listViewComponent.createObject(page1);
            if (isMobileLayout) {
                listView.anchors.left = listView.parent.left;
                listView.anchors.top = listView.parent.top;
                listView.anchors.bottom = listView.parent.bottom;
                listView.width = listView.parent.width;
            } else {
                listView.anchors.left = listView.parent.left;
                listView.anchors.top = listView.parent.top;
                listView.anchors.bottom = listView.parent.bottom;
                listView.width = listView.parent.width / 3;
            }

            if (listView === null) {
                // Error Handling
                console.log("Error creating object");
            }
        }
    }

    function archiveItem(item_id) {
        var requestComponent = Qt.createComponent("components/MarkAsReadRequest.qml");
        var request = requestComponent.createObject(null, {"apiKey": apiKey, "accessToken":accessToken, "itemId": item_id});
        request.refresh();
        if (isMobileLayout === false) {
            detailView.destroy();
//            detailView.givenUrl = "";
//            detailView.itemId = "";
        } else {
            pageStack.pop();
        }
    }

    function deleteItem(item_id) {
        var requestComponent = Qt.createComponent("components/DeleteRequest.qml");
        var request = requestComponent.createObject(null, {"apiKey": apiKey, "accessToken":accessToken, "itemId": item_id});
        request.refresh();
        if (isMobileLayout === false) {
            detailView.destroy();
//            detailView.givenUrl = "";
//            detailView.itemId = "";
        } else {
            pageStack.pop();
        }
    }

    function favoriteItem(item_id) {
        var requestComponent = Qt.createComponent("components/FavoriteRequest.qml");
        var request = requestComponent.createObject(null, {"apiKey": apiKey, "accessToken":accessToken, "itemId": item_id});
        request.refresh();
    }

    function addItem(url) {
        var requestComponent = Qt.createComponent("components/AddItemRequest.qml");
        var request = requestComponent.createObject(null, {"apiKey": apiKey, "accessToken":accessToken, "url": url});
        request.refresh();
    }

    function showSheet(title, message) {
        var sheet = PopupUtils.open(defaultSheet);
        sheet.doneButton = false;
        sheet.title = title;
        sheet.message = message;
        sheet.contentsHeight = units.gu(1);
    }

    Component {
        id: defaultSheet

        DefaultSheet {
            property alias message: sheetLabel.text

            title: title
            doneButton: false
            Label {
                id: sheetLabel
                anchors.fill: parent
                text: message
                wrapMode: Text.WordWrap
            }
            onDoneClicked: PopupUtils.close(sheet)
        }
    }
}
