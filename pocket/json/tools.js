.pragma library

function getIcon(name) {
    return Qt.resolvedUrl("/usr/share/icons/ubuntu-mobile/actions/scalable/" + name + ".svg")
}

function addValue(key,value,first) {
    if(value !== "") {
        if (first === true) {
            return "\""+key+"\":\""+value+"\"";
        } else {
            return ",\""+key+"\":\""+value+"\"";
        }
    } else {
        return "";
    }
}
