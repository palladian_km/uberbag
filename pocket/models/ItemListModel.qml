import QtQuick 2.0
import "../json"
import "../json/tools.js" as JSTools

JSONListModel {

    // parameters supported by the Pocket JSON API.
    property string apiKey
    property string accessToken
    property string state
    property bool favorite
    property string tag
    property string contentType
    property string sort
    property string detailType
    property string search
    property string domain
    property string since
    property int count
    property int offset

    source: "https://getpocket.com/v3/get?consumer_key=" + apiKey + "&access_token="+accessToken + "&detailType=complete"
    postData: {
        if(apiKey !== "" && accessToken !== "") {
            var data = "{\"consumer_key\":\""+apiKey+"\",\"access_token\":\""+accessToken+"\"";

            data += JSTools.addValue("state",state,false);
            data += JSTools.addValue("favorite",favorite ? 1 : 0,false);
            data += JSTools.addValue("tag",tag,false);
            data += JSTools.addValue("contentType",contentType,false);
            data += JSTools.addValue("sort",sort,false);
            data += JSTools.addValue("detailType",detailType,false);
            data += JSTools.addValue("search",search,false);
            data += JSTools.addValue("domain",domain,false);
            data += JSTools.addValue("since",since,false)
            data += JSTools.addValue("count",count,false);
            data += JSTools.addValue("offset",offset,false);

            data += "}";
            console.log(data);
            return data;
        } else {
            throw "Not successfully authenticated.";
        }
    }
    query: "$.list.*"
    requestType: "GET"
}
