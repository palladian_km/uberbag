import QtQuick 2.0
import Ubuntu.Components.Extras.Browser 0.2
import Ubuntu.Components 1.1;
Rectangle {
    property string givenUrl
    property string itemId

    UbuntuWebView {
        id: webView
        anchors.fill: parent

        preferences.standardFontFamily: "Ubuntu"

        onUrlChanged: {
            console.log(url)
        }

        url: givenUrl
    }

    ActivityIndicator {
        id: loadingIndicator
        running: webView.loading
        anchors.top: parent.top
        anchors.right: parent.right
    }

    function reload () {
        webView.reload();
    }
}
