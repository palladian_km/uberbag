import QtQuick 2.0
import Ubuntu.Components 1.1

Page {
    property string givenUrl
    property string itemId

    DetailView {
        id: itemDetailView
        anchors.fill: parent
        givenUrl: parent.givenUrl
        itemId: parent.itemId
    }

    function reload () {
        itemDetailView.reload();
    }
}
