import QtQuick 2.0
import Ubuntu.Components 1.1
import Ubuntu.Components.Popups 1.0

DefaultSheet {
    property string message

    id: sheet
    title: title
    doneButton: true
    Label {
        id: sheetLabel
        anchors.fill: parent
        text: message
        wrapMode: Text.WordWrap
    }
    onDoneClicked: PopupUtils.close(sheet)
}
