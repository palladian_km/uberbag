import QtQuick 2.0
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 1.0
import "../models"
import "../json/jsonpath.js" as JSONPath

UbuntuListView {

    property string accessToken: mainView.accessToken;

    id: items
    clip: true

    model: ListModel {

    }

    ItemListModel {
        id: pocketModel
        apiKey: "24758-96f4f9ca2d5918b0cd65fa0d"
        detailType: "complete"
        contentType: "image"

        Component.onCompleted: {
            items.model = model;
        }
    }

    onAccessTokenChanged: {
        if (accessToken !== "") {
            pocketModel.accessToken = accessToken;
            pocketModel.refresh();
        }
    }

    // PocketSubtitled is identical to a Subtitled, but with a maximum line count of 3.
    // Thereby we circumvent the dreaded layouting problems with longer sub texts.
    delegate: PocketSubtitled {
        text: { if (resolved_title !== "") return resolved_title;
                else return given_url;}
        subText: excerpt
        iconSource: JSONPath.jsonPath(images, "$.1.src")[0]
        progression: true

        onClicked: {
            if (mainView.isMobileLayout === false) {
                // desktop layout
                if (detailView === null) {
                    var detailViewComponent = Qt.createComponent("DetailView.qml");
                    detailView = detailViewComponent.createObject(page1);
                    detailView.anchors.left = items.right;
                    detailView.anchors.right = page1.right;
                    detailView.anchors.top = page1.top;
                    detailView.anchors.bottom = page1.bottom;

                    // this fixes the often faulty placement of the detail view when anchoring it to the page's top -> it seems there is a bug
                    // in QT which causes the page's coordinate system's origin to be placed without considering the header
                    if (mapFromItem(page1, detailView.x, detailView.y).y === mapFromItem(mainView, detailView.x, detailView.y).y) {
                        detailView.anchors.topMargin = page1.header.height;
                    }
                }
                console.log("given_url=" + model.given_url);
                detailView.givenUrl = model.given_url;
                detailView.itemId = model.item_id;
            } else {
                // mobile layout
                pageStack.push(detailPage);
                detailPage.givenUrl = model.given_url;
                detailPage.itemId = model.item_id;
            }
        }
    }

    function refresh() {
        if (!mainView.isMobileLayout && detailView !== null) {
            // workaround to make sure, that the web view responds to URL changes after refreshing (see #14)
            detailView.destroy();
        }
        pocketModel.refresh();
    }
}
