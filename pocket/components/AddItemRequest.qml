import QtQuick 2.0
import "../json"

JSONListModel {
    property string url
    property string title
    property string tags
    property string apiKey
    property string accessToken

    source: "https://getpocket.com/v3/add?consumer_key=" + apiKey + "&access_token="+accessToken+"&url="+url
//    postData: {
//        if(apiKey !== "" && accessToken !== "") {
//            var data = "{\"consumer_key\":\""+apiKey+"\",\"access_token\":\""+accessToken+"\","
//            data += addValue("url", url, true);
//            data += addValue("title", title, false);
//            data += addValue("tags", tags, false);

//            data = data + "}";
//            console.log(data);
//            return data;
//        } else {
//            throw "Not successfully authenticated.";
//        }
//    }
    query: "$"
//    requestType: "POST"

    function addValue(key,value,first) {
        if(value !== "") {
            if (first === true) {
                return "\""+key+"\":\""+value+"\"";
            } else {
                return ",\""+key+"\":\""+value+"\"";
            }
        } else {
            return "";
        }
    }

    onListModelUpdateChanged: {
        console.log(model.status);
        listView.refresh();
    }

    onErrorMessageChanged: {
        console.log("Add request failed with error message " + errorMessage);
        showSheet("Info", "Error adding\n\n " + url + "\n\nto your pocket list...\n\nError message was:\n" + errorMessage + "\n\nMake sure that you copy a URL to your clipboard before clicking on 'Add'.");
    }
}
