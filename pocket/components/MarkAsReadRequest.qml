import QtQuick 2.0
import "../json"

JSONListModel {
    property string itemId
    property string time
    property string apiKey
    property string accessToken

    source: "https://getpocket.com/v3/send?consumer_key=" + apiKey + "&access_token="+accessToken+"&actions=%5B%7B%22action%22%3A%22archive%22%2C%22item_id%22%3A"+itemId+"%7D%5D"
//    postData: {
//        if(apiKey !== "" && accessToken !== "") {
//            var data = "{\"consumer_key\":\""+apiKey+"\",\"access_token\":\""+accessToken+"\",\"actions\":[{"

//            data += addValue("action","archive", true);
//            data += addValue("item_id",itemId, false);
//            data += addValue("time",time, false);

//            data = data + "}]}";
//            console.log(data);
//            return data;
//        } else {
//            throw "Not successfully authenticated.";
//        }
//    }
    query: "$.list.*"
//    requestType: "POST"

    onListModelUpdateChanged: {
        console.log(model.action_results);
        listView.refresh();
    }
}
